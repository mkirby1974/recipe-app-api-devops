output "db_host" {
  value = aws_db_instance.main.address
}

output "api_endpoint" {
  value = aws_lb.api.dns_name
}
