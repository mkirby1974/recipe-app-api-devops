resource "aws_db_subnet_group" "main" {
  name       = "${local.prefix}-main"
  subnet_ids = data.aws_subnet_ids.db.ids

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_security_group" "rds" {
  name   = "${local.prefix}-rds-inbound-access"
  vpc_id = data.aws_vpc.main.id
  ingress     {
    from_port = 5432
    protocol  = "tcp"
    to_port   = 5432

    security_groups = [
      aws_security_group.ecs_service.id,
    ]
  }
  
  tags = local.common_tags
}

resource "aws_security_group_rule" "rds-bastion-rule" {
  type              = "ingress"
  cidr_blocks       = ["${data.aws_instance.bastion.private_ip}/32"]
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  security_group_id = aws_security_group.rds.id
}

resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db"
  name                    = "recipe"
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "11.10"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  multi_az                = false
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
