terraform {
  backend "s3" {
    bucket         = "sandbox-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_vpc" "main" {
  filter {
    name   = "tag-key"
    values = ["sandbox"]
  }
}

data "aws_subnet_ids" "dmz" {
  vpc_id = data.aws_vpc.main.id
  tags = {
    dmz = true
  }
}

data "aws_subnet_ids" "db" {
  vpc_id = data.aws_vpc.main.id
  tags = {
    db = true
  }
}

data "aws_subnet_ids" "app" {
  vpc_id = data.aws_vpc.main.id
  tags = {
    app = true
  }
}

data "aws_instance" "bastion" {

  filter {
    name   = "tag:Name"
    values = ["bastion-rv-sandbox-sandbox"]
  }
}
